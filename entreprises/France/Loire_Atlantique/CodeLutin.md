# CodeLutin

site : <https://www.codelutin.com/>


## Brève Description

Petite SSLL militante de 15 salariés. Spécialisée dans les technologies Java et web (html/CSS/javascript).

### Détails

L'entreprise est membre fondateur du réseau alliances libre sur Nantes et membre du réseau libre-entreprise. 
Elle suit donc une charte qui promouvois notamment l'égalité au sein de l'entreprise ett la transparence.  

## Stages et/ou entretiens passés : oui/oui

 * stage TN09 pour le développement de Timebundle, logiciel de gestion de temps de travail en équipe (stagiaire Tobias OLLIVE)
 
## Mots-clés

 * html/css/javascript
 * java
 * militant
 
## Contacts

	- tel : +33 2 40 50 29 28
	- mail : stage@codelutin.com
	- adresse : 12 avenue Jules Verne 44230 Saint-Sebastien-sur-Loire 	

# MIND

site : https://www.mind.be/en/


## Brève Description

SSII dans le monde de l'embarqué, mind est une filliale de ESSENSIUM. Elle est spécialisée dans 
le développement open source.

### Détails

Mind provides consultancy and services (outsourcing and fixed price projects), as well as support 
and training, in the field of Linux, Android and Open Source software for embedded systems. 
Within this field, Mind's core expertise is in Low-level software development, Networking, 
Security and Multimedia.
Our team consists of highly skilled and enthusiastic embedded software engineers with a passion 
for Open Source software and for hardware.

## stages et/ou entretiens passés : NON/NON

 
## Mots-clés

 * open-source
 * embarqué
 
## Contacts

	- tel : +32-16-28.65.00
	- mail : contact@mind.be
	- adresse : Gaston Geenslaan 9 B-3001 Leuven Belgium 
	

# Explication

Ce dépôt a pour but de rassembler le travail de tous ceux qui cherchent ou ont cherché des stages TN09 ou TN10 en informatique dans un environnement de travail de logiciel libre, au moins de développement open source. 

Ce dépôt git vient du constat que les seules offres proposées par l'UTC au moment d'effectuer une recherche de stage sont des offres dans des entreprises traditionnelles aux mœurs pas toujours en accord avec les valeurs du libre ou de la protection des utilisateurs. Les étudiants et étudiantes cherchant des stages différents ont alors fort à faire simplement pour trouver des entreprises répondant à leurs exigences éthiques.

# Contribution et Organisation du dépôt

Pour contribuer il suffit d'ajouter une nouvelle fiche obéissant au template [suivant](./template_entreprise.md) et de la placer dans la section appropriée.
Merci de faire bien attention au template, le script est loin d'être robuste actuellement.
 
 Le dépôt est organisé selon l'arborescence suivante : 
 - Entreprise
    - Étranger
     - Europe
     - Autre
    - France
     - Région
 - Site internet
    
Ensuite, si vous disposez d'un PC sous Linux, vous pouvez exécuter ce [script](./output/setup.sh) pour mettre à jour la liste générale des entreprises listées pour l'instant [ici](./output/liste.md)

## Amélioration futures

 *   [x] améliorer le script qui pour l'instant est extêmement basique et ne permet aucune option
 *   [x] automatiser l'exécution du script à chaque commit
 *   [ ] publier les résultats sur le wiki et sortir une version pdf ou html
 *   [ ] ajouter des scripts pour trier les entreprises par mots-clés
 *   [ ] ajouter d'autres entreprises
    
## N'hésitez pas à contribuer 
Merci beaucoup

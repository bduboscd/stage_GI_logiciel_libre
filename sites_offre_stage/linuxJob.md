# LinuxJobs

<https://linuxjobs.fr>

## description

Site francais d'offres d'emploi principalement mais quelques offres de stage aussi. Les technologies utilisées 
seront normalement FOSS mais les boîtes ne sont pas forcément militantes et ne développent pas forcement que du
code libre.

Ne pas hésiter à candidater aux offres d'emploi en offre de stage spontanée.

## experience(s) personnelle(s)

 * j'ai réussi à avoir un entretien avec une boîte qui ne m'a finalement pas pris.

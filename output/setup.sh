# ce programme crée une liste résumé du dépot en markdown pour que ce soit lisible rapidement
# la fonction liste les fichiers du répertoires et s'appelle pour les sous répertoires
# elle est appellée de la façon suivante : listfolder path lvl
function  listfolder() {
    #path=$1 # chemin d'appel du fichier
    lvl=$(($lvl+1)) # le niveau est augmenté de 1
    listfiles=$(ls $1)
    echo $listfiles
    # on parcours toutes les lignes du ls
    for line in $listfiles
    do
        # si c'est un fichier
        if [ -d "$1/$line" ] ; then
            # debug
            echo "path folder : $1/$line ## level : $lvl" > $info 
            # on affiche le nombre de # correspondant au niveau
            printNTime $lvl '#' $file
            # affichage du nom du dossier
            echo " $line" >> $file
echo "--------------------------" >> $file
            # appel récursif de la fonction
            listfolder "$1/$line" $lvl
        else
            # on parse la feuille 
            echo "fichier à parser : $1/$line ## level : $lvl" > $info
            parseFile "$1/$line" $lvl
        fi
    done
    lvl=$(($lvl-1))
}



#usage parsefile addr_fichier niveau
function parseFile(){
    # on remet à 0 les listes
    action="rien"
    keywordList=""
    echo $action
    echo $keywordList
    while read md_line; do
        #echo $md_line
        #compte le nombre de dièse en début de ligne
        lvlTitle=$(echo $md_line | grep -o  "#" | wc -l)
        #echo $lvlTitle
        # si égal à 1 ou à 2
        if [ $lvlTitle -le 2 ] && [ $lvlTitle -gt 0 ] ; then
            # si c'est 1, c'est le nom de la boite
            #echo $md_line
            if [ $lvlTitle -eq 1 ] ; then 
            # affichage du nom du dossier
            printNTime $2 '#' $file
            # ajout de lien locaux [here](./README_2.md) # It works! TODO
            echo $1
            echo " [${md_line:2}]($1)" >> $file
            #echo "${md_line:2}"
            else
                firstWord=$(expr "$md_line" : '\(## \S*\)')
                valtest=${firstWord:3}
                # a voir avec le case, je n'ai pas réussi à le faire marcher
                echo "${valtest,,}"
                if [ $(expr "${valtest,,}" = "brève") -eq 1 ] ; then
                        #echo "description mode"
                        action=description
                        echo "**Description**" >> $file
                    
                fi
                
                if [ $(expr "${valtest,,}" = "stages") -eq 1 ] ; then # on met la valeur en minuscule
                oui=$(echo "${md_line,,}" | grep -o "oui.*")  # affichage de la chaine commençant par oui
                        
                nboui=$(echo "${oui,,}" | grep -o oui | wc -l) # récupération du nombre de oui dans la chaine
                        if [ $(expr length $oui) -gt 3 ] ; then # si la chaine contient deux mots
                            stage="X"   # le stage est forcément à oui
                            if [ $nboui -eq 2 ] ; then # s'il y a deux oui
                                entretien="X"
                            else
                                entretien=" "
                            fi 2>$debug
                        else
                            stage=" "
                            if [ nboui -eq 1 ] ; then # s'il y a un oui
                                entretien="X"
                            else
                                entretien=" "
                                fi 2>$debug
                        fi 2>$debug
                        echo "**Contact passés avec l'entreprises** " >> $file
                        echo "entretien(s) passé(s) : [$entretien] stage(s) effectué(s) : [$stage] " >> $file
                fi
                
                if [ $(expr "${valtest,,}" = "mots-clés") -eq 1 ] ; then
                        action=keyword
                        echo "" >> $file
                        printf " **Mots-clés** : " >> $file
                fi

                if [ $(expr "${valtest,,}" = "contacts") -eq 1 ] ; then
                        echo $keywordList >> $file
                        echo "" >> $file
                        action=address
                fi
            fi
        else if [ $lvlTitle -eq 0 ] ; then
            case $action in
            
                description)
                    echo $md_line >> $file # ajouter la description
                ;;
                
                keyword)
                    index=$(expr index "$md_line" \*) 2>$debug # on cherche une ligne avec un *
                    index=$(($index+1)) 2>$debug # on passe l'étoile
                    if [ "$index" -ne 1 ] ; then # si il y avait un * dans la ligne
                        keywordList=$(echo "$keywordList ${md_line:$index}") # ajout du mot clé à la liste
                    fi
                ;;
                address)
                    adresse=$(echo $md_line | grep -o "adresse.*") # on cherche la ligne avec adresse
                    if [  "$(expr length "$adresse")" -gt 7  ] ; then # si on l'a trouvé
                    adresse=${adresse:9} # récupération de l'adresse
                    adresselink=${adresse// /%20} # remplacement de \ \ par %20
                    # affichage adresse + lien sur recherche openstreetmap
                    echo "**Adresse** : [$adresse](https://openstreetmap.org/search?query=$adresselink)" >> $file 
                    fi 2>$debug
                ;;
            esac
            else
                action="rien" # pour raz la condition
            fi
                
        fi
    done < $1

}

# usage : printNtime N mot Sortie
function printNTime() {
            #affichage du bon nombre de # devant le mot
            # affichage d'abord de s puis remplacement
            printf "%$1s"  | sed s/' '/"$2"/g >> $3


}



################################# DÉBUT DU SCRIPT##########################

#-------------------- initialisation des variables du script-------------
# localisation du fichier de sortie
file="liste.md"
# localisation de la racine de l'arborescence
root="../entreprises"
# sortie de l'erreur standart
info="/dev/null"
debug="/dev/null" #sortie d'erreur cachée
# niveau de profondeur de l'arboresence
lvl=1
#--------------------------------fonctions pour getops----------------------

function usage(){
printf "script qui parse le repo git local pour creer un fichier markdown récapitulant les entreprises \n"
printf " usage :\n"
printf "setup.sh [OPTIONS]\n"
printf "\t -h --help                : montre ce message \n"
printf "\t -o --output              : spécifie le fichier de sortie // pas encore implementé \n"
printf "\t -i --input               : spécifie la localisation du dossier racine \n"
printf "\t -d --debug               : affiche les erreurs d'execution\n"
exit 100
}


function setOutput() {
    file=$1
}

function setInput() {
    root=$1
}

function showerror() {
    info="/dev/stdout"
    if [ $(expr $info = "/dev/stdout") ] ; then
        debug="/dev/stderr"
    fi
}

#---------------------fonction for script options------------------------
OPTS=$( getopt -o i:,h,o:,d -l --debug,help,output:,input, -- "$@" )

eval set -- "$OPTS"

while true ; do
    case $1 in 
    -h) usage;
    exit 0;;
    -o) setOutput $2;
    shift 2;;
    -i) setInput  $2;
    shift 2;;
    -d) showerror; # je ne sais pas si ca marche
    shift;;
    --help) usage;
    exit 0;;
    --output) setOutput $2;
    shift 2;;
    --input) setInput $2;
    shift 2;;
    --debug) showerror; # je ne sais pas si ça marche
    shift;;
    --) shift ; break;;
    esac
done

#----------------------début du script------------------------------------
# ecriture de la première ligne du fichiers, on ecrase un eventuel fichier précedent
echo "# listes d'entreprises" > $file
# on rajoute la date d'écriture du fichier
echo $(date) >> $file
        
listfolder $root

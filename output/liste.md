# listes d'entreprises
dim. juin 10 12:27:18 CEST 2018
## Etranger
--------------------------
### Autre
--------------------------
### Europe
--------------------------
#### Belgique
--------------------------
##### [MIND](../entreprises/Etranger/Europe/Belgique/mind.md)
**Description**

SSII dans le monde de l'embarqué, mind est une filliale de ESSENSIUM. Elle est spécialisée dans
le développement open source.

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [ ] stage(s) effectué(s) : [ ] 

 **Mots-clés** : open-source embarqué

**Adresse** : [ Arenberg Science Park Gaston Geenslaan 9 B-3001 Leuven Belgium](https://openstreetmap.org/search?query=%20Arenberg%20Science%20Park%20Gaston%20Geenslaan%209%20B-3001%20Leuven%20Belgium)
#### Republique_Tcheque
--------------------------
##### [Codasip](../entreprises/Etranger/Europe/Republique_Tcheque/Codasip.md)
**Description**

Based in the heart of Europe (Czech Republic), Codasip is a rapidly growing startup focused on redefining embedded processing for the IoT era through groundbreaking IP and automation technology.

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [ ] stage(s) effectué(s) : [ ] 

 **Mots-clés** : RISC_V embarqué

**Adresse** : [ Božetěchova 1/2 612 00 Brno](https://openstreetmap.org/search?query=%20Božetěchova%201/2%20612%2000%20Brno)
## France
--------------------------
### Bouches_du_Rhone
--------------------------
#### [Biblibre](../entreprises/France/Bouches_du_Rhone/biblibre.md)
**Description**

BibLibre a été fondé sur une idée : aider les bibliothèques, de tout type, à déployer des logiciels sous licence libre.

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [ ] stage(s) effectué(s) : [ ] 

 **Mots-clés** : bibliothèque militant

**Adresse** : [ 108 rue Breteuil 13006 MARSEILLE](https://openstreetmap.org/search?query=%20108%20rue%20Breteuil%2013006%20MARSEILLE)
### Ile_et_Vilaine
--------------------------
#### [savoir-faire linux](../entreprises/France/Ile_et_Vilaine/savoir-faireLinux.md)
**Description**

Savoir-faire Linux est expert en technologies libres et open source au Canada et en Europe. Fondée sur l’économie du savoir, l’entreprise est spécialisée en technologies de l’information, objets connectés et ingénierie logicielle.

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [ ] stage(s) effectué(s) : [ ] 

 **Mots-clés** : militant embarqué web progiciel IA

**Adresse** : [ 14 rue Dupont des Loges 35000 Rennes](https://openstreetmap.org/search?query=%2014%20rue%20Dupont%20des%20Loges%2035000%20Rennes)
### Loire_Atlantique
--------------------------
#### [CodeLutin](../entreprises/France/Loire_Atlantique/CodeLutin.md)
**Description**

Petite SSLL militante de 15 salariés. Spécialisée dans les technologies Java et web (html/CSS/javascript).

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [X] stage(s) effectué(s) : [X] 

 **Mots-clés** : html/css/javascript java militant

**Adresse** : [ 12 avenue Jules Verne 44230 Saint-Sebastien-sur-Loire](https://openstreetmap.org/search?query=%2012%20avenue%20Jules%20Verne%2044230%20Saint-Sebastien-sur-Loire)
### Vaucluse
--------------------------
#### [bootlin](../entreprises/France/Vaucluse/free_electrons.md)
**Description**

Bootlin (anciennement Free Electrons) est une société d’ingénierie spécialisée en Linux embarqué
et plus généralement en logiciel libre et open-source pour systèmes embarqués.

**Contact passés avec l'entreprises** 
entretien(s) passé(s) : [ ] stage(s) effectué(s) : [ ] 

 **Mots-clés** : militant embarqué buildroot hardware

**Adresse** : [ 136 avenue Maréchal Foch 84100 Orange](https://openstreetmap.org/search?query=%20136%20avenue%20Maréchal%20Foch%2084100%20Orange)

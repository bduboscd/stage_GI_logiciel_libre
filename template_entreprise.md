# $NOM_RENTREPRISE

site : <$LIEN_SITE_ENTREPRISE>


## Brève Description (1 ligne seulement)

$MON_SUPER_RÉSUMÉ

### Détails

$MA_DESCRIPTIO_PLUS_DÉTAILLÉE

## stages et/ou entretiens passés : ${OUI/NON}/${OUI/NON}

 * $QUOI_QUI_POURQUOI_QUAND_COMMENT
 
## Mots-clés

 * $SPEC1
 * $SPEC2
 
## Contacts

	- tel : $NUMÉRO
	- mail : $ADRESSE_MAIL
	- adresse : $NUMÉRO $RUE $CODE_POSTAL $VILLE 
	
